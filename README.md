# docker-tuto

## Présentation de Docker

Docker est avant tout une société mais également une technologie de conteneurisation, ce dernier utilise la technologie de conteneurisation qui permet la création et l'utilisation de conteneurs Linux. 
Nous pouvons ainsi utiliser les conteneurs comme des machines virtuelles mais contrairement au VM docker est très modulaires, léger et facile à déployer. 

Docker se base sur un modèle de création d'image, il est ainsi plus simple de partager une application avec toutes leurs dépendances, entre plusieurs environnements grâce à l'image Docker.
L'instanciation d'une ou plusieurs images Docker donne naissance à un conteneur ou plusieurs conteneurs. 
Il faut imaginer que un conteneur Docker est un une boîte invisible avec tout ce dont il a besoin pour s’exécuter. Cela comprend le système d’exploitation, le code de l’application, les outils système et les librairies


## Installation de Docker sur Ubuntu
en cours de rédaction

## DockeFile
Vous trouverez les images Docker sur le registre Dockerhub, c'est le registre contenant les images open source publique ou privé. Il ressemble à Gitlab ou Github.

## Les commandes Docker à connaitre

``` 
docker images 
```

La commande docker images liste toutes les images docker enregistré en local, à noter que docker image ls fait également le même boulot.


```
docker pull redis
```
Par défaut la commande pull télécharge l'images à partir de Dockerhub, mais vous pouvez également élécharger sur un registre privé.
Noter que vous pouvez préciser le numéro de tag (version) de l'image que vous souhaitez télécharger ex: docker pull redis:5.4 par défaut si vous ne précisez rien il récupère la dernière version qui est nommé latest.


```
docker run redis
```
La commande run permet de lancer votre conteneur en lui précisant le nom de l'image, noter que si l'image n'existait pas en local Docker le téléchargera automatiquement. 
Dans notre exemple on instancie un conteneur avec l'image de redis, par défaut si vous lancer juste docker run redis, le conteneur portera un nom aléatoire et sera lancerer en foreground. 
Si vous souhaitez lancer en background préciser l'argument -d qui veut dire detached.
Cette commande est essentielle, elle comporte beaucoup d'arguments non détaillé tel que le network, le volume etc... je vous recommande de lire la doc sur le site de Docker

```
docker exec -it mon_conteneur_php73 php -v
```

Cette commande permet de se connecter directement a un conteneur déjà lancé. Elle prend en argument le nom du conteneur et la commande que vous voulez lancer à partir du conteneur.
Dans notre exemple la commande docker exec -it mon_conteneur_php73 php -v va lancer php dans le conteneur et afficher à l'écran la version de php car c'est ce qu'on lui demande de faire :)


```
docker stop redis
```
Lorsque vous exécuter la commande docker run docker lance un conteneur pour la première fois. Si vous voulez arrêter le conteneur, faites docker stop nom_du_conteneur

```
docker start redis
```
Lance un conteneur a l'arrêt.

```
docker restart redis
```

Relance un conteneur déjà lancé.


## Dépendance de service
Imaginons que vous avez un conteneur php qui est lancé mais ce dernier à besoin d'une base de donnée mysql, d'un cache, d'un serveur web, on va aborder la notion de dépendance.

## L'outil docker-compose pour faciliter le lancement des conteneurs
en cours de rédaction